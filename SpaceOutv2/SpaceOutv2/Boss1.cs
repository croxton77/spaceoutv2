﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceOutv2
{
    public class Boss1
    {
        Texture2D image;
        Texture2D missleL;
        Texture2D missleR;
        Texture2D laser;

        public int hp = 15;

        Vector2 velocity ;
        Vector2 position;
        Vector2 nxtPos;
        Vector2 shotSpeed = new Vector2(0f, 6f);
        public List<Bullet> _lasers = new List<Bullet>();

        float waitTime;
        float waitTime2;

        private bool missleLock = true;

        public Bullet[] _missles = new Bullet[4];

        const int LWALL = 0;
        const int RWALL = 512;
        

        public Rectangle boundingBox
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);
            }
        }

        public Boss1(Texture2D img, Texture2D missleLight, Texture2D missleDark, Texture2D lazer, Vector2 pos)
        {
            image = img;
            missleL = missleLight;
            missleR = missleDark;
            position = pos;
            laser = lazer;

            velocity = new Vector2(5f, 0f);

            _missles[0] = new Bullet(missleL, position + new Vector2(0f, 20f), new Vector2(0, 0));
            _missles[1] = new Bullet(missleL, position + new Vector2(14f, 32f), new Vector2(0, 0));
            _missles[2] = new Bullet(missleR, position + new Vector2(42f, 32f), new Vector2(0, 0));
            _missles[3] = new Bullet(missleR, position + new Vector2(56f, 20f), new Vector2(0, 0));
            
        }

        public void Update(GameTime gametime)
        {
            Move();
            for (int i = 0; i < 4; i++) _missles[i].Update();

            if (waitTime == 0 && missleLock)
            {
                waitTime = gametime.TotalGameTime.Seconds + 2;
                waitTime2 = gametime.TotalGameTime.Seconds + 4f;
            }
            else if (waitTime != 0)
            {
                if (waitTime == gametime.TotalGameTime.Seconds)
                {
                    missleLock = false;
                    waitTime = 0;
                }
            }
            if (waitTime2 != 0)
            {
                if (waitTime2 == gametime.TotalGameTime.Seconds)
                {
                    Reload();
                    waitTime2 = 0;
                }
            }
            for (int i = 0; i < _lasers.Count; i++) if(_lasers[i].OnScreen())_lasers[i].Update();
        }

        private void Move()
        {
            nxtPos = position + velocity;
            if (nxtPos.X > LWALL && nxtPos.X + image.Width < RWALL)
            {
                position = nxtPos;
                for (int i = 0; i < 4; i++) 
                    if(!_missles[i].fired)_missles[i].position += velocity;  

            }
            else velocity.X *= -1;
            

        }

        public void Draw(SpriteBatch spritebatch)
        {
            for(int i=0; i <4; i++)
                if (_missles[i].OnScreen()) _missles[i].Draw(spritebatch);
            spritebatch.Draw(image, position, Color.White);
            for (int i = 0; i < _lasers.Count; i++) if(_lasers[i].OnScreen())_lasers[i].Draw(spritebatch);
        }

        public Vector2 getPos()         //returns bottom center of this Boss
        {
            Vector2 center;

            center.X = boundingBox.Center.X;
            center.Y = boundingBox.Bottom;
            return (center);
        }

        public void Launch(int missle)
        {
          
            _missles[missle].velocity = new Vector2(0f, 9f);
            _missles[missle].fired = true;
            missleLock = true;
            
          
        }

        public bool Target(Vector2 player)
        {
            int distance;
            int ship = (int)player.X;
            int Alien = (int)getPos().X;
            distance = ship - Alien;

            if (distance < -27 && distance > -33 &&!missleLock &&!_missles[0].fired)
            {
                Launch(0);
                return true;
            }
            if (distance < -12 && distance > -18 &&!missleLock &&!_missles[1].fired)
            {
                Launch(1);
                return true;
            }
            if (distance > 12 && distance < 18 && !missleLock && !_missles[2].fired)
            {
                Launch(2);
                return true;
            }
            if (distance > 27 && distance < 33 && !missleLock && !_missles[3].fired)
            {
                Launch(3);
                return true;
            }
            if (distance > -3 && distance < 3)
            {
                _lasers.Add(new Bullet(laser, getPos(), shotSpeed));
                return true;
            }

            else return false;
                
        }

        public void Reload()
        {
            if (!_missles[0].OnScreen())
            {
                _missles[0].fired = false;
                _missles[0].velocity = new Vector2(0f, 0f);
                _missles[0].position = position + new Vector2(0f, 20f);
               
            }

            if (!_missles[1].OnScreen())
            {
                _missles[1].fired = false;
                _missles[1].velocity = new Vector2(0f, 0f);
                _missles[1].position = position + new Vector2(14f, 32f);
            }

            if (!_missles[2].OnScreen())
            {
                _missles[2].fired = false;
                _missles[2].velocity = new Vector2(0f, 0f);
                _missles[2].position = position + new Vector2(42f, 32f);
            }
            if (!_missles[3].OnScreen())
            {
                _missles[3].fired = false;
                _missles[3].velocity = new Vector2(0f, 0f);
                _missles[3].position = position + new Vector2(56f, 20f);
            }
        }
    }
}
