﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceOutv2
{
    public class Bullet
    {
        public Vector2 position;
        public Vector2 velocity;
        private Texture2D image;
        public bool fired = false;

        const int ROOF = 0;
        const int FLOOR = 512;

        public Rectangle boundingBox
        {
            get
            {
                return new Rectangle((int)position.X,(int)position.Y,image.Width,image.Height);

            }
        }

        

        public Bullet(Texture2D img, Vector2 pos,Vector2 vel)
        {
            image = img;
            position = pos;
            velocity = vel;
        }

        public void Update()
        {
            position = position + velocity;
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(image, position, Color.White);
        }

        public bool OnScreen()
        {
            if (boundingBox.Bottom < ROOF || boundingBox.Bottom> FLOOR) return false;
            else return true;
        }

    }
}
