﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace SpaceOutv2
{
    public class Ship
    {
        public Vector2 position;
        private Vector2 velocity;
        private Vector2 nxtPos;
        public int moveSpeed;
        public Vector2 shotSpeed;
        Texture2D image;
        public bool visible = false;
        public List<Bullet> _bullets = new List<Bullet>();
       
       

        const int LWALL = 0;
        const int RWALL = 512;

        public Rectangle boundingBox
        {
            get
            {
                return new Rectangle((int)position.X,(int)position.Y, image.Width, image.Height);
            }
        }

        public Ship(Texture2D img, Vector2 pos)
        {
            image = img;
            position = pos;
            velocity = new Vector2(0f, 0f);
            moveSpeed = 1;
            shotSpeed = new Vector2(0f, -5f);
        }

        public void Update()
        {
            Move();
            nxtPos = velocity *moveSpeed + position;
            if (nxtPos.X > LWALL && nxtPos.X +image.Width < RWALL) position = nxtPos;

  

        }

        private void Move()
        {
            if (Keyboard.GetState().IsKeyUp(Keys.Left) && Keyboard.GetState().IsKeyDown(Keys.Right)) velocity.X = 5;
            if (Keyboard.GetState().IsKeyDown(Keys.Left) && Keyboard.GetState().IsKeyUp(Keys.Right)) velocity.X = -5;
            if (Keyboard.GetState().IsKeyDown(Keys.Left) && Keyboard.GetState().IsKeyDown(Keys.Right)) velocity.X = 0;
            if (Keyboard.GetState().IsKeyUp(Keys.Left) && Keyboard.GetState().IsKeyUp(Keys.Right)) velocity.X = 0;
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(image, position, Color.White);
        }

        public Vector2 getPos()         //returns top center of the ship
        {
            Vector2 center;

            center.X = boundingBox.Center.X;
            center.Y = boundingBox.Top;
            return (center);
        }

        public void Reset(Vector2 spawn)
        {
            position = spawn;
        }

        public bool Shoot(Texture2D img)
        {
            if (_bullets.Count < 3)
            {
                _bullets.Add(new Bullet(img, getPos(), shotSpeed));
                return true;
            }
            else return false;
        
        }

       

    }
}
