using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SpaceOutv2
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        //properties
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        KeyboardState oldState;

        Vector2 shipSpawn = new Vector2(216f, 490f);
        Vector2 shipPos;  //may bot be needed remember to check later
        Vector2 bombPos;
        Ship player;            //go back and replace is visible bool with null
        Bomb bomb;
        SpriteFont font;
        Boss1 boss;

        List<Brick> _bricks = new List<Brick>();
        List<RedAlien> _redAlien = new List<RedAlien>();
        List<OrangeAlien> _orangeAlien = new List<OrangeAlien>();
        List<DisplayElement> _display = new List<DisplayElement>();
        List<Bomb> _bombs = new List<Bomb>();

  
        int level;
        int lives;
        int enemies;

        Texture2D shipImg;
        Texture2D bombImg;
        Texture2D redBrick;
        Texture2D orangeBrick;
        Texture2D RedAlien;
        Texture2D orangeAlien;
        Texture2D redShot;
        Texture2D orangeShot;
        Texture2D startScreen;
        Texture2D gameOver;
        Texture2D backgroung;
        Texture2D Gaurdimg;
        Texture2D missleLight;
        Texture2D missleDark;
        Texture2D blueShotBig;

       

        SoundEffect playerShot;
        SoundEffect RedAlienShot;
        SoundEffect brickPop;
        SoundEffect ballHit;


        const int maxRed = 20;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 512;           //set screen height
            graphics.PreferredBackBufferWidth = 512;            //set screen with
            Content.RootDirectory = "Content";
        }

        /// <summary>
        ///
        /// Allows the game to perform any initialization it needs to before starting to run
        /// 
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            oldState = Keyboard.GetState();
            
            level = 6;
            lives = 3;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            shipImg = Content.Load<Texture2D>("playerSprites/ship");
            bombImg = Content.Load<Texture2D>("playersprites/ball");
            redBrick = Content.Load<Texture2D>("levels/redBrick");
            RedAlien = Content.Load<Texture2D>("enemys/RedAlien");
            redShot = Content.Load<Texture2D>("bullet1");
            startScreen = Content.Load<Texture2D>("levels/startscreen");
            gameOver = Content.Load<Texture2D>("levels/gameoverscreen");
            backgroung = Content.Load<Texture2D>("levels/background");
            playerShot = Content.Load<SoundEffect>("sounds/playerShoot");
            RedAlienShot = Content.Load<SoundEffect>("sounds/enemyShoot");
            brickPop = Content.Load<SoundEffect>("sounds/brickPop");
            ballHit = Content.Load<SoundEffect>("sounds/Ball_hit");
            orangeBrick = Content.Load<Texture2D>("levels/orangeBrick");
            orangeAlien = Content.Load<Texture2D>("enemys/orangeAlien");
            orangeShot = Content.Load<Texture2D>("enemys/orangeBullet");
            font = Content.Load<SpriteFont>("font1");
            Gaurdimg = Content.Load<Texture2D>("enemys/BOSS1");
            missleDark = Content.Load<Texture2D>("enemys/missleDark");
            missleLight = Content.Load<Texture2D>("enemys/missleLight");
            blueShotBig = Content.Load<Texture2D>("enemys/blueShotBig"); 
            
           
            levelBuilder(level);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
                // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                || Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
          
            // TODO: Add your update logic here

            UpdateInput();
            //update player and enimies
            if (player !=null && player.visible)player.Update();
            for(int i =0; i < _bombs.Count;i++) _bombs[i].Update();
            for (int i = 0; i < _redAlien.Count; i++) _redAlien[i].Update();
            for (int i = 0; i < _orangeAlien.Count; i++) _orangeAlien[i].Update(gameTime);
            if (boss != null) boss.Update(gameTime);

            //updates all bullets
            if(player !=null)
            for (int i = 0; i < player._bullets.Count; i++) 
            {
                player._bullets[i].Update();
                if(!player._bullets[i].OnScreen()) player._bullets.RemoveAt(i);
            }
            for (int i = 0; i < _redAlien.Count; i++)
                for (int j = 0; j < _redAlien[i]._bullets.Count; j++)
                {
                    _redAlien[i]._bullets[j].Update();
                    if (!_redAlien[i]._bullets[j].OnScreen()) _redAlien[i]._bullets.RemoveAt(j);
                }
                    
            //checks for collision and tells enemies to fire
            ShotCheck();
            CollisionCheck();

            if (bomb != null && bomb.held) Magnet(bomb, player);
            if (bomb != null && !bomb.OnScreen()) Die();

            enemies = _redAlien.Count + _orangeAlien.Count;
            if (enemies == 0 && player != null && player.visible && boss == null)
            {
                level += 1;
                levelBuilder(level);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //  drawing code here
            spriteBatch.Begin();

            for (int i = 0; i < _display.Count; i++) _display[i].Draw(spriteBatch);
            if(player !=null && player.visible) player.Draw(spriteBatch);
            for(int i =0; i<_bombs.Count;i++)_bombs[i].Draw(spriteBatch);
            if(player !=null) for (int i = 0; i < player._bullets.Count; i++) player._bullets[i].Draw(spriteBatch);
            for (int i = 0; i < _bricks.Count; i++) _bricks[i].Draw(spriteBatch);
            for (int i = 0; i < _redAlien.Count; i++) _redAlien[i].Draw(spriteBatch);
            for (int i = 0; i < _orangeAlien.Count; i++) _orangeAlien[i].Draw(spriteBatch);

            if (boss != null) boss.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void Magnet(Bomb ball, Ship ship)        //keeps bomb on ship until launched
        {
            shipPos = ship.getPos();
            bombPos.X = shipPos.X - bombImg.Width/2;
            bombPos.Y = shipPos.Y - bombImg.Height -1;
            ball.position = bombPos;
        }





        public void CollisionCheck()
        {
            // BALL COLLISION CHECK BEGINS HERE

            if (bomb !=null && bomb.boundingBox.Intersects(player.boundingBox)) //checks if ball hits player
            {
                bomb.Bounce(player);
            }

            for(int i =0; i<_bricks.Count;i++)      //checks if ball hits brick
            {
               if (bomb.boundingBox.Intersects(_bricks[i].boundingBox))
               {
                   bomb.Hit(_bricks[i]);
                    if(_bricks[i].Hit())_bricks.RemoveAt(i);
                    brickPop.Play();
               }
           }
            for (int i = 0; i < _redAlien.Count; i++)      //checks if ball hits RedAlien
            {
                if (bomb.boundingBox.Intersects(_redAlien[i].boundingBox))
                {
                    bomb.Hit(_redAlien[i]);
                    _redAlien.RemoveAt(i);
                    ballHit.Play();

                }
            }
            for (int i = 0; i < _orangeAlien.Count; i++)      //checks if ball hits OrangeAlien
            {
                if (bomb.boundingBox.Intersects(_orangeAlien[i].boundingBox))
                {
                    bomb.Hit(_orangeAlien[i]);
                    _orangeAlien.RemoveAt(i);
                    ballHit.Play();

                }
            }
            if (boss != null && bomb.boundingBox.Intersects(boss.boundingBox))
            {
                bomb.Hit(boss);
                boss.hp -= 3;
                ballHit.Play();
            }

            //PLAYER'S BULLET COLLISION CHECK BEGINS HERE

            if(player !=null)
            for (int i = 0; i < player._bullets.Count; i++)        
                for (int j = 0; j < _bricks.Count; j++)     //checks if bullet hits brick
                {
                    if (player._bullets[i].boundingBox.Intersects(_bricks[j].boundingBox))
                    {
                        player._bullets.RemoveAt(i);
                        break;
                    }
                }
            
            if(player !=null)
            for (int i = 0; i < player._bullets.Count; i++)
                for(int j = 0; j<_redAlien.Count; j++) // checks if bullt hits RedAlien
                {
                    if (player._bullets[i].boundingBox.Intersects(_redAlien[j].boundingBox))
                    {
                        player._bullets.RemoveAt(i);
                        _redAlien.RemoveAt(j);
                        ballHit.Play();
                        break;
                    }
                }

            if (player != null)
            for (int i = 0; i < player._bullets.Count; i++)
                for (int j = 0; j < _orangeAlien.Count; j++) // checks if bullt hits OrangeAlien
                {
                    if (player._bullets[i].boundingBox.Intersects(_orangeAlien[j].boundingBox))
                    {
                        player._bullets.RemoveAt(i);
                        _orangeAlien[j].hp -= 1;
                        if (_orangeAlien[j].hp == 0)
                        {
                            ballHit.Play();
                            _orangeAlien.RemoveAt(j);
                        }
                        else brickPop.Play();
                        break;
                    }
                }   

            //RedAlien BULLET COLLISION CHECK BEGINS HERE
            for (int i = 0; i < _redAlien.Count; i++)
                for (int j = 0; j < _redAlien[i]._bullets.Count; j++)
                    for (int k = 0; k < _bricks.Count; k++)     //checks if bullet hits brick
                        {
                            if (_redAlien[i]._bullets[j].boundingBox.Intersects(_bricks[k].boundingBox))
                            {
                                _redAlien[i]._bullets.RemoveAt(j);
                                break;
                            }
                        }

            for (int i = 0; i < _redAlien.Count; i++)
                for (int j = 0; j < _redAlien[i]._bullets.Count; j++)
                    for (int k = 0; k < _bombs.Count; k++)       //checks if bullet hits bomb
                    {
                        if(_redAlien[i]._bullets[j].boundingBox.Intersects(_bombs[k].boundingBox))
                        {
                            _redAlien[i]._bullets.RemoveAt(j);
                            break;
                        }
                    }

            for (int i = 0; i < _redAlien.Count; i++)   //checks if bullet hits player
                for (int j = 0; j < _redAlien[i]._bullets.Count; j++)
                    if (_redAlien[i]._bullets[j].boundingBox.Intersects(player.boundingBox))
                    {
                        _redAlien[i]._bullets.RemoveAt(j);
                        Die();
                        break;
                    }
            //ORANGE ALIEN BULLET COLLISION CHECK BEGINS HERE
            for (int i = 0; i < _orangeAlien.Count; i++)        //checks if bullet hits brick
                for (int j = 0; j < 3; j++)
                    for(int k=0; k <_bricks.Count;k++)
                    {
                        if (_orangeAlien[i]._bullets[j].boundingBox.Intersects(_bricks[k].boundingBox))
                        {
                            _orangeAlien[i]._bullets[j].position.Y += 512;
                            break;
                        }
                    }

            for (int i = 0; i < _orangeAlien.Count; i++)        //checks if bullet hits bomb
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < _bombs.Count; k++)
                    {
                        if (_orangeAlien[i]._bullets[j].boundingBox.Intersects(_bombs[k].boundingBox))
                        {
                            _orangeAlien[i]._bullets[j].position.Y += 512;
                            break;
                        }
                    }
            for (int i = 0; i < _orangeAlien.Count; i++)        //checks if bullet hits player
                for (int j = 0; j < 3; j++)
                    {
                        if (_orangeAlien[i]._bullets[j].boundingBox.Intersects(player.boundingBox))
                        {
                            _orangeAlien[i]._bullets[j].position.Y += 512;
                            Die();
                            break;
                        }
                    }   
               //boss1 collision chack begins here

                 for (int i = 0; i < boss._lasers.Count; i++)              //checks if laser hits brick
                    for (int j = 0; j < _bricks.Count; j++)
                    {
                        if (boss._lasers[i].boundingBox.Intersects(_bricks[j].boundingBox))
                        {
                            boss._lasers.RemoveAt(i);
                            break;
                        }
                    }


                  for (int i = 0; i < boss._lasers.Count; i++)         //checks if laser hits bomb
                    for (int j = 0; j < _bombs.Count; j++)
                    {
                        if (boss._lasers[i].boundingBox.Intersects(_bombs[j].boundingBox))
                        {
                            boss._lasers.RemoveAt(i);
                            break;
                        }
                    }
                for(int i=0;i< boss._lasers.Count; i++)
                    if (boss._lasers[i].boundingBox.Intersects(player.boundingBox))
                    {
                        Die();
                        boss._lasers.RemoveAt(i);
                        break;
                    }
                for (int i=0;i<4;i++)
                    if (boss._missles[i].boundingBox.Intersects(player.boundingBox))
                    {
                        boss._missles[i].position.Y += 512;
                       // Die();
                        break;
                    }
            
                    

                 
        }





        public void ShotCheck()  //determines if 
        {
            for (int i = 0; i < _redAlien.Count; i++)
                if (_redAlien[i].Target(player.getPos()) && _redAlien[i].Shoot(redShot)) //reminder go back and move bullets inside of redenemy class;
                    RedAlienShot.Play();
            
            for (int i = 0; i < _orangeAlien.Count; i++)
                if(_orangeAlien[i].Target(player.getPos()))
                    RedAlienShot.Play();
            if (boss != null && boss.Target(player.getPos())) RedAlienShot.Play();

               
            
   
        }

        public void levelBuilder(int lev)       //places all objects for required level.
        {
            if (lev == 0) //start screen
            {
                ClearScreen();
                lives =3;
                _display.Add( new DisplayElement(startScreen, new Vector2(0f,0f)));
            }

            if (lev == 101) //game over screen
            {
                ClearScreen();
                _display.Add(new DisplayElement(gameOver, new Vector2(0f, 0f)));
            }

            if (lev == 1)   //level 1
            {
                ClearScreen();
                _display.Add(new DisplayElement(backgroung, new Vector2(0f, 0f)));
                DrawLives();

               
                player = new Ship(shipImg, shipSpawn);
                player.visible = true;
                _bombs.Add(new Bomb(bombImg, player, true));
                bomb = _bombs[0];

               _redAlien.Add(new RedAlien(RedAlien,new Vector2(0f,50f)));

            }

            if (lev == 2)   //level 2
            {
                ClearScreen();
                _display.Add(new DisplayElement(backgroung, new Vector2(0f, 0f)));
                DrawLives();


                player = new Ship(shipImg, shipSpawn);
                player.visible = true;
                _bombs.Add(new Bomb(bombImg, player, true));
                bomb = _bombs[0];

                _redAlien.Add(new RedAlien(RedAlien, new Vector2(0f, 50f)));
                //layer 1
                _bricks.Add(new Brick(1, new Vector2(30f, 150f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(90f, 150f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(150f, 150f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(210f, 150f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(270f, 150f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(330f, 150f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(390f, 150f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(450f, 150f), redBrick));
                //layer2
                _bricks.Add(new Brick(1, new Vector2(60f, 180f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(120f, 180f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(180f, 180f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(240f, 180f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(300f, 180f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(360f, 180f), redBrick));
                _bricks.Add(new Brick(1, new Vector2(420f, 180f), redBrick));
        
       

            }

            if (lev == 3)   //level 3
            {
                ClearScreen();
                _display.Add(new DisplayElement(backgroung, new Vector2(0f, 0f)));
                DrawLives();


                player = new Ship(shipImg, shipSpawn);
                player.visible = true;
                _bombs.Add(new Bomb(bombImg, player, true));
                bomb = _bombs[0];

                _redAlien.Add(new RedAlien(RedAlien, new Vector2(0f, 50f)));
                _redAlien.Add(new RedAlien(RedAlien, new Vector2(300f, 90f)));
                //layer 1
                //layer 1
                _bricks.Add(new Brick(2, new Vector2(30f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(90f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(150f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(210f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(270f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(330f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(390f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(450f, 150f), redBrick, orangeBrick));
                //layer2
                _bricks.Add(new Brick(2, new Vector2(60f, 180f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(120f, 180f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(180f, 180f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(240f, 180f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(300f, 180f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(360f, 180f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(420f, 180f), redBrick, orangeBrick));
            }

            if(lev ==4) // level 4
            {
                ClearScreen();
                _display.Add(new DisplayElement(backgroung, new Vector2(0f,0f)));
                DrawLives();


                player = new Ship(shipImg, shipSpawn);
                player.visible = true;                                  //reminder move this to initiialize phase of ship class;
                _bombs.Add(new Bomb(bombImg, player, true));
                bomb = _bombs[0];                                       // is this neccesarry???

                _orangeAlien.Add( new OrangeAlien(orangeAlien,orangeShot, new Vector2(0f,50f)));
            }

            if (lev == 5) // level 5
            {
                ClearScreen();
                _display.Add(new DisplayElement(backgroung, new Vector2(0f, 0f)));

                player = new Ship(shipImg, shipSpawn);
                player.visible = true;
                _bombs.Add(new Bomb(bombImg, player, true));
                bomb = _bombs[0];

                _redAlien.Add(new RedAlien(RedAlien, new Vector2(206f, 30f)));
                _orangeAlien.Add(new OrangeAlien(orangeAlien, orangeShot, new Vector2(0f, 100f)));
                _orangeAlien.Add(new OrangeAlien(orangeAlien, orangeShot, new Vector2(480f, 120f)));

                _bricks.Add(new Brick(2, new Vector2(200f, 70f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(230f, 70f), redBrick, orangeBrick));
                _bricks.Add(new Brick(2, new Vector2(260f, 70f), redBrick, orangeBrick));

                _bricks.Add(new Brick(1, new Vector2(420f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(1, new Vector2(450f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(1, new Vector2(390f, 150f), redBrick, orangeBrick));

                _bricks.Add(new Brick(1, new Vector2(30f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(1, new Vector2(60f, 150f), redBrick, orangeBrick));
                _bricks.Add(new Brick(1, new Vector2(90f, 150f), redBrick, orangeBrick));

            }

            if (lev == 6) //level 6
            {
                ClearScreen();
                _display.Add(new DisplayElement(backgroung, new Vector2(0f, 0f)));

                player = new Ship(shipImg, shipSpawn);
                player.visible = true;
                _bombs.Add(new Bomb(bombImg, player, true));
                bomb =_bombs[0];

              
                boss = new Boss1(Gaurdimg, missleLight, missleDark, blueShotBig, new Vector2(100f, 20f));

                for (int i = 2; i < 490; i += 30)
                {
                    //_bricks.Add(new Brick(1, new Vector2(i, 110f),redBrick));
                    //_bricks.Add(new Brick(1, new Vector2(i, 130f), redBrick));
                    //_bricks.Add(new Brick(1, new Vector2(i, 150f), redBrick));
                    //_bricks.Add(new Brick(1, new Vector2(i, 170f), redBrick));
                    //_bricks.Add(new Brick(1, new Vector2(i, 190f), redBrick));
                    //_ bricks.Add(new Brick(1, new Vector2(i, 210f), redBrick));
                }
            }
                 

        }

        public void UpdateInput()
        {
            KeyboardState newState = Keyboard.GetState();

            if (newState.IsKeyDown(Keys.Space) && !oldState.IsKeyDown(Keys.Space))  //detects if space pressed
            {
                if (bomb.held) bomb.Launch();
                else if (player != null && player.visible)
                {
                    if(player.Shoot(redShot))
                        playerShot.Play();
                }
            }
            if (newState.IsKeyDown(Keys.Enter) && !oldState.IsKeyDown(Keys.Enter))
            {
                if (level == 0)
                {
                    level = 1;
                    levelBuilder(level);
                }
                if (level == 101)
                {
                    level = 0;
                    levelBuilder(level);
                }
                
            }
            oldState = newState;
        }
       
     

        public void ClearScreen()
        {
            if(player !=null)player.visible = false;
            _bombs.Clear();
            if(player != null)player._bullets.Clear();
            for (int i=0; i < _redAlien.Count; i++) _redAlien[i]._bullets.Clear();
            _redAlien.Clear();
            _display.Clear();
            _bricks.Clear();
            _orangeAlien.Clear();
        }

        public void Die()
        {
            player._bullets.Clear();
            for (int i =0; i < _redAlien.Count; i++) _redAlien[i]._bullets.Clear();
            bomb.Reset();
            player.Reset(shipSpawn);
            lives -=1;
            DrawLives();
            if (lives == 0)
            {
                level = 101;
                levelBuilder(level);
            }
        }

        public void DrawLives()
        {
            _display.RemoveAll(DisplayElement => DisplayElement.image == shipImg);
            if (lives >1) _display.Add(new DisplayElement(shipImg,new Vector2(0f,0f)));
            if (lives > 2) _display.Add(new DisplayElement(shipImg, new Vector2(50f, 0f))); 
        }

    }
}
