﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace SpaceOutv2
{
    public class Bomb
    {
        public Vector2 position;
        public Vector2 velocity;
        private Vector2 nxtPos;
        int speed;
        Texture2D image;

        const int LWALL = 0;
        const int RWALL = 512;
        const int ROOF = 0;
        const int FLOOR = 512;


        Random RandomGenerator = new Random();

        public Rectangle boundingBox
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);
            }
        }

        public Boolean held;


        public Bomb(Texture2D img, Ship ship, Boolean hold)
        {
            image = img;
            held = hold;
            speed = 1;
   

            if (held) velocity = new Vector2(4f, 0f);

        }

        public void Update()
        {
            Move();
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(image, position, Color.White);
        }

        public void Launch() 
        {
            velocity.X = RandomGenerator.Next(5, 15) - 10;
            velocity.Y = Math.Abs(velocity.X) -8 ;
            held = false;
        }

        public void Bounce(Ship player)
        {
            int distance = boundingBox.Center.X - player.boundingBox.Left;

            if (distance <= 5) velocity.X = -5;
            else if (distance > 5 && distance <= 20) velocity.X = -3;
            else if (distance > 20 && distance <= 24) velocity.X = -1;
            else if (distance > 24 && distance <= 26) velocity.X = 0;
            else if (distance > 26 && distance <= 30) velocity.X = 1;
            else if (distance > 30 && distance <= 45) velocity.X = 3;
            else if (distance > 45) velocity.X = 5;

            velocity.Y = Math.Abs(velocity.X) - 8;
        }

        public void Hit(Brick brick)        //note to self change to accept a rectangle to avoid the repitition
        {
            if (boundingBox.Top + 10 >= brick.boundingBox.Bottom) velocity.Y = Math.Abs(velocity.Y);
            else if (boundingBox.Bottom - 10<= brick.boundingBox.Top) velocity.Y = Math.Abs(velocity.Y) *-1;
            else if (boundingBox.Right - 10 <= brick.boundingBox.Left) velocity.X = Math.Abs(velocity.X) * -1;
            else if (boundingBox.Left + 10 >= brick.boundingBox.Right) velocity.X = Math.Abs(velocity.X);

        }

        public void Hit(RedAlien Alien)
        {
            if (boundingBox.Top + 10 >= Alien.boundingBox.Bottom) velocity.Y = Math.Abs(velocity.Y);
            else if (boundingBox.Bottom - 10 <= Alien.boundingBox.Top) velocity.Y = Math.Abs(velocity.Y) * -1;
            else if (boundingBox.Right - 10 <= Alien.boundingBox.Left) velocity.X = Math.Abs(velocity.X) * -1;
            else if (boundingBox.Left + 10 >= Alien.boundingBox.Right) velocity.X = Math.Abs(velocity.X);

        }

        public void Hit(OrangeAlien Alien)
        {
            if (boundingBox.Top + 10 >= Alien.boundingBox.Bottom) velocity.Y = Math.Abs(velocity.Y);
            else if (boundingBox.Bottom - 10 <= Alien.boundingBox.Top) velocity.Y = Math.Abs(velocity.Y) * -1;
            else if (boundingBox.Right - 10 <= Alien.boundingBox.Left) velocity.X = Math.Abs(velocity.X) * -1;
            else if (boundingBox.Left + 10 >= Alien.boundingBox.Right) velocity.X = Math.Abs(velocity.X);

        }
        public void Hit(Boss1 Alien)
        {
            if (boundingBox.Top + 10 >= Alien.boundingBox.Bottom) velocity.Y = Math.Abs(velocity.Y);
            else if (boundingBox.Bottom - 10 <= Alien.boundingBox.Top) velocity.Y = Math.Abs(velocity.Y) * -1;
            else if (boundingBox.Right - 10 <= Alien.boundingBox.Left) velocity.X = Math.Abs(velocity.X) * -1;
            else if (boundingBox.Left + 10 >= Alien.boundingBox.Right) velocity.X = Math.Abs(velocity.X);

        }

        private void Move()
        {
            nxtPos = position + velocity * speed;
            if (nxtPos.X > LWALL && nxtPos.X +image.Width < RWALL && nxtPos.Y > ROOF) position = nxtPos;
            if (nxtPos.X <= LWALL || nxtPos.X + image.Width >= RWALL) velocity.X *= -1;
            if (nxtPos.Y <= ROOF) velocity.Y *= -1;

        }

        public void Reset()
        {
            velocity = new Vector2(0f, 0f);
            held = true;
        }

        public bool OnScreen()
        {
            if (position.Y > FLOOR) return false;
            else return true;
        }
    }
}
