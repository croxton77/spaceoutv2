﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace SpaceOutv2
{
   public class Brick
    {
        int hitPoints;
        Texture2D stage1;
        Texture2D stage2;
        Texture2D image;
        Vector2 position = new Vector2(-100f,-100f);

        public Rectangle boundingBox
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);
            }
        }

        public Brick(int hP, Vector2 pos, Texture2D stg1)
        {
            hitPoints = hP;
            position = pos;
            stage1 = stg1;
            setImage();
        }
        public Brick(int hP, Vector2 pos, Texture2D stg1, Texture2D stg2)
        {
            hitPoints = hP;
            position = pos;
            stage1 = stg1;
            stage2 = stg2;
            setImage();
        }

        public void Update()
        {

        }

        public void Draw(SpriteBatch spritebatch)
        {
            setImage();
            spritebatch.Draw(image, position, Color.White);
        }
        private void setImage()
        {
            if (hitPoints == 1) image = stage1;
            else if (hitPoints == 2) image = stage2;
        }
        public bool Hit()
        {
            hitPoints -= 1;
            setImage();
            if (hitPoints == 0) return true;
            else return false;
        }
    }
}
