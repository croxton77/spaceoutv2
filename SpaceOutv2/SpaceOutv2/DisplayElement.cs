﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceOutv2
{
    class DisplayElement
    {

        public Texture2D image;
        Vector2 position;


        public DisplayElement(Texture2D img, Vector2 pos)
        {
            image = img;
            position = pos;
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(image, position, Color.White);
        }
        
    }
}
