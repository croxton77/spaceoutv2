﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;


namespace SpaceOutv2
{
    public class RedAlien
    {
        private Vector2 position;
        private Vector2 velocity;
        private Vector2 nxtPos;
        public int speed;
        private Vector2 shotSpeed = new Vector2(0f, 5f);
        private bool targeted = true;
        public List<Bullet> _bullets = new List<Bullet>();
        private Texture2D image;

        public Rectangle boundingBox
        {
            get
            {
                return(new Rectangle((int)position.X,(int)position.Y,image.Width,image.Height));
            }
        }



    
        private Texture2D bullet;

        const int LWALL= 0;
        const int RWALL = 512;

        public RedAlien(Texture2D img, Vector2 pos)
        {
            image = img;
            position = pos;
            velocity = new Vector2(3f,0f);
            speed = 1;
        }
        public RedAlien()
        {
            velocity = new Vector2(3f,0f);

        }

        public void Update()
        {
            Move();
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(image, position, Color.White);
            for (int i = 0; i < _bullets.Count; i++) _bullets[i].Draw(spritebatch);
        }

        private void Move()
        {
            nxtPos = position + velocity *speed;
            if (nxtPos.X > LWALL && nxtPos.X + image.Width < RWALL) position = nxtPos;
            else velocity.X *= -1;
        }

        public Vector2 getPos()         //returns bottom center of this RedAlien
        {
            Vector2 center;

            center.X = boundingBox.Center.X;
            center.Y = boundingBox.Bottom;
            return (center);
        }

        public bool Shoot(Texture2D img)
        {
            if (_bullets.Count < 3)
            {
                _bullets.Add(new Bullet(img, getPos(), shotSpeed));
                return true;
            }
            else return false;

        }

        public bool Target(Vector2 player)
        {

            int ship = (int)player.X;
            int Alien = (int)getPos().X;
            if (Math.Abs(ship - Alien) < 4 && targeted)
            {
                targeted = false;
                return true;

            }
            else
            {
                targeted = true;
                return false;
            }
        }

    }
}
