﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace SpaceOutv2
{
    public class OrangeAlien 
    {
        private Vector2 position;
        private Vector2 velocity;
        private Vector2 nxtPos;
        public int speed;
        private Vector2 shotSpeed = new Vector2(0f, 0f);
        public Bullet[] _bullets = new Bullet[3];
        public float waitTime = 0;
        public float waitTime2;
        private bool superShot =false;
        public int hp;
      


        public Rectangle boundingBox
        {
            get
            {
                return(new Rectangle((int)position.X,(int)position.Y,image.Width,image.Height));
            }
        }
       


        private Texture2D image;
        private Texture2D bullet;

        const int LWALL= 0;
        const int RWALL = 512;

        public OrangeAlien(Texture2D img, Texture2D bulletimg, Vector2 pos)
        {
            image = img;
            bullet = bulletimg; 
            position = pos;
            velocity = new Vector2(3f,0f);
            speed = 1;
            hp = 2;

            _bullets[0] = new Bullet(bullet, position + new Vector2(0F, 11F), shotSpeed);
            _bullets[1]= new Bullet(bullet, position+ new Vector2(11F,14F),shotSpeed);
            _bullets[2]= new Bullet(bullet, position + new Vector2(22F, 12F), shotSpeed);
        }
        
        public void Update(GameTime gametime)
        {
            Move();
            for (int i = 0; i < _bullets.Length; i++) 
                if(_bullets[i].OnScreen())_bullets[i].Update();

            if (_bullets[0].fired && _bullets[1].fired && _bullets[2].fired && !superShot)
            {
                superShot=true;
                waitTime = 3 +(float)gametime.TotalGameTime.Seconds ;
            }
            
            else if (waitTime != 0)      //waits thrree seconds then fires all three shots
            {
                if (waitTime == (float)gametime.TotalGameTime.Seconds)
                {
                    Reload();
                    Shoot(0); Shoot(1); Shoot(2);
                    superShot = true;
                    waitTime = 0;
                    waitTime2 = 2 + gametime.TotalGameTime.Seconds;
                }
            }
            if (waitTime2 !=0) //waits an additiona; 2 seconds then reloads
            {
                 if (waitTime2 == gametime.TotalGameTime.Seconds)
                 {
                     waitTime2 = 0;
                     Reload();
                     superShot = false;
                 }
            }

        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(image, position, Color.White);
            for (int i = 0; i < _bullets.Length; i++) 
                if(_bullets[i].OnScreen())_bullets[i].Draw(spritebatch);
        }

        private void Move()
        {
            nxtPos = position + velocity *speed;
            if (nxtPos.X > LWALL && nxtPos.X + image.Width < RWALL) 
            {
                position = nxtPos;
                for (int i = 0; i < _bullets.Length; i++)
                    if (!_bullets[i].fired) _bullets[i].position += velocity; 
            }
            else velocity.X *= -1;
        }

        public Vector2 getPos()         //returns bottom center of this RedAlien
        {
            Vector2 center;

            center.X = boundingBox.Center.X;
            center.Y = boundingBox.Bottom;
            return (center);
        }

        public void Shoot(int bul)
        {
            if (bul == 0)
            {
                _bullets[0].velocity = new Vector2(-2f, 5f);
                _bullets[0].fired = true;
            }
            if (bul == 1)
            {
                _bullets[1].velocity = new Vector2(0f, 5f);
                _bullets[1].fired = true;
            }
            if (bul == 2)
            {
                _bullets[2].velocity = new Vector2(2f, 5f);
                _bullets[2].fired = true;
            }

          
        }

        public bool Target(Vector2 player)
        {
            int distance;
            int ship = (int)player.X;
            int Alien = (int)getPos().X;
            distance = ship - Alien;

            if (distance < -171 && distance > -179 && !_bullets[0].fired)
            {
                Shoot(0);
                return true;
            }
            if (Math.Abs(distance) < 4 && !_bullets[1].fired)
            {
                Shoot(1);
                return true;
            }
           
            if(distance >171 && distance <179 &&!_bullets[2].fired)
            {
                Shoot(2);
                return true;
            }

            else return false;
            
        }

        public void Reload()
        {
            for (int i = 0; i < 3; i++)
            {
                _bullets[i].fired = false;
                _bullets[i].velocity = shotSpeed;
            }
            _bullets[0].position = position + new Vector2(0F, 11F);
            _bullets[1].position = position + new Vector2(11f,14f);
            _bullets[2].position = position + new Vector2(22f,12f);

        }

    }
}
